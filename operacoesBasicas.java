/*Java*/

int a = 5;
int b = 3;
int sum = a + b;
int product = a * b;
int quotient = a / b;
int difference = a - b;

System.out.println("Sum: " + sum);
System.out.println("Product: " + product);
System.out.println("Quotient: " + quotient);
System.out.println("Difference: " + difference);

for (int i = 0; i < 5; i++) {
    System.out.println("Iteration: " + i);
}

int num = 10;

if (num > 5) {
    System.out.println("Number is greater than 5");
} else if (num == 5) {
    System.out.println("Number is equal to 5");
} else {
    System.out.println("Number is less than 5");
}

import java.util.ArrayList;
import java.util.List;

List<Integer> numbers = new ArrayList<>();
numbers.add(1);
numbers.add(2);
numbers.add(3);

for (int num : numbers) {
    System.out.println(num);
}
