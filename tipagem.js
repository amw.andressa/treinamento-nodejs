// JavaScript é de tipagem dinâmica
// JavaScript é dita como untyped
// JavaScript é multiparadigma, ou seja, problemas diferentes podem ter várias formas de resolução.
// JavaScript se chama EcmaScript, maneira formal
// É uma linguagem interpretada
// Linguagem interpretada = O código é lido e interpretado linha por linha em tempo de execução.
// Linguagem compilada = O código é compilado, transformado em linguagem de máquina primeiro e depois esse arquivo gerado em linguagem de máquina é executado.
// NodeJS é o ambiente de execução de código JavaScript fora do navegador. Só isso.
// O NodeJS na verdade é uma ferramenta que interpreta o JavaScript
// O nodeJS é a ferramenta que faz com que o o JavaScript possa ser 'rodado' em um ambiente fora do navegador.
// NodeJS foi criado para que fosse possível que o JavaScript fosse utilizado fora do navegador, como sempre foi.
// O NodeJS foi criado para isso.
// O console do navegador seria o "terminal" do JavaScript
// NodeJS é o ambiente para gerar código JavaScript fora do navegador.
// Ninguém programa em Node, se pograma em JavaScript.

let minhaVar = 567;
minhaVar = "Texto";
minhaVar = true;