// Exercício 1: Soma de Números
// Crie um programa que recebe dois números como entrada e exibe a soma deles.

// function soma(){
//     return 2 + 2;
// }

// parâmetros x argumento
// ordem dos parâmetros

// function nomeidade(nome, idade){
//     return `meu nome é ${nome} e minha idade é ${idade}`;
// }

// console.log(nomeidade("Juliana", 40));

// parâmetros de função -> é o que ela recebe na declaração
// argumento da função -> é o que ela recebe na hora de ser chamada
// function soma(num1, num2){
//     return num1 + num2;
// }

// function multiplica(numero1, numero2){
//     return numero1 * numero2;
// }

// forçando não gerar erro caso 
// esqueça de chamar algum parâmetro ao chamar a função
// function multiplica(numero1 = 1, numero2 = 1){
//     return numero1 * numero2;
// }

// boa prática criar funções que recebem poucos parâmetros

// console.log(multiplica(soma(4,5), soma(3,3)));
// console.log(multiplica(soma(4,5)));

function cumprimentar(nome){
    return `Olá, ${nome}`;
}

console.log(cumprimentar("Andressa"));

function comParametro(param) {
    console.log(param)
}
comParametro()