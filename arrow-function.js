// Nome de arquivo no js é utilizado o padrão nome-segundoNome.
// nome de funções e variaveis dentro do arquivo é usado o padrão
// camelCase

function apresentar(nome){
    return `meu nome é ${nome}`
}

// Arrow function -> não pode ser noomeada, sempre vem com
// uma const e o nome da variável que vamos receber de parâmetro.
const apresentarArrow = nome => `meu nome é ${nome}`;
const soma = (num1, num2) => num1 + num2;

// Arrow function com mais d euma linha de instrução

const somaNumerosPequenos = (num1, num2) => {
    if (num1 || num2 > 10){
        return "Somente números de 1 a 9"
    } else {
        return num1 + num2;
    }
}

// Hoisting: arrow function se comporta como expressão, 
// é lido antes da mesma forma de var e as outras funções