/*
    Variáveis tipo 'var' é pouco usada por é muito propensa a bugs pois deixa o código muito 'solto' com muita liberdade
    Variáveis tipo 'let
    -> Pode ser declarado sem inicializar nada dentro
    Variáveis tipo 'const' são constantes e não variáveis, é necessário inicializar antes.
    e depois de inicializada ela não vai mudar nunca mais.
 */

const forma = 'triangulo';
const altura = 5;
const comprimento = 7;
let area;

if(forma =='quadrado'){
    area = altura * comprimento;
} else {
    area = (altura * comprimento) / 2;
}
console.log(area);