// == comparação implícita

const numero = 5;
const texto = "5";


console.log(numero == texto);
// Resultado true porque o js fez a conversão do 
// texto para número antes e depois comparou.

// === comparação explícita
// ele não converte automáticamente
// compara o valor e os tipos da variáveis.

const numero1 = 6;
const texto1 = "6";
console.log(numero === texto);

console.log(typeof numero1);
console.log(typeof texto);

// == compara só o valor
// === compara o valor e o tipo de dado
// as boas práticas pedem para usar somente os 3 iguais
//!= e !== Operadores “não igual” e “estritamente não igual”, utilizados para comparação, 
// da mesma forma que == e === retornam true ou false.
// e a conversão deve ser feita de maneira explícita com os métodos
// Number() e String() ou .toString()

