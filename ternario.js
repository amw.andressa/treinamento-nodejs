const idadeMinima = 18;
const idadeCliente = 19;


console.log("Com if else: ")
if(idadeCliente >= idadeMinima){
    console.log("cerveja");
} else {
    console.log("suco");
}
console.log("Com operador ternario: ")
console.log(idadeCliente >= idadeMinima ? "cerveja" : "suco");

// dica de boa prática não usar vários operadores ternários dentro de outros operadores ternários.
// para condições aninhadas é sempre melhor para leitura e para manutenção posterior do código
// o uso do if else
// CURIOSIDADE: chama de operador ternário porque ele tem três operadores na mesma linha
// que são eles: >=  ?  :

