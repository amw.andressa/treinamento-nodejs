// tipo de dado
//booleanos

//conversão implícita
// = Atribuição
// == Comparação entre conteúdo, o que está dentro da variável
// === Comparação entre valor e tipo

// const numero = 456;
// const numeroString = "456";

// if(!(numero === numeroString)){
//     console.log("Verdadeiro ou falso? ", "Falso");
// } else {
//     console.log("Verdadeiro ou falso? ", "Verdadeiro");
// }

//Number();
//String();

//const numero = 456;
// Conversão explícita
//const numeroString = Number("456a");

// console.log(numero + Number(numeroString));
// console.log(numero + numeroString);

// let telefone = 12341234;
// console.log("O telefone é " + telefone);
// console.log(typeof telefone);

var respostaDeTudo = 42
let idade = 29
const pi = 3.14

{
    var respostaDeTudo = 3.14
    let idade = 42
    const pi = 29
    console.log(respostaDeTudo, idade, pi)

}
console.log(respostaDeTudo, idade, pi)