// expressao de funcao


// diferença principal: HOISTING
// funções e var são "listadas" no topo
// do arquivo. Ou seja, são lidos primeiro.

console.log(soma(1, 1));

console.log(apresentar());
function apresentar(){
    return "olá";
}

console.log(soma(1,1));
const soma = function(num1, num2){
    return num1 + num2
}