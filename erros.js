// Não é possível mudar o valor da const,
// então por isso é preciso que o seu valor seja tribuído na 
// declaração da variável.
// O NodeJS executa no servidor e não no navegador.

// const numero = 1;
// const minhaVar = 'oi';
// console.log(minhaVar);

// ReferenceError; - Variável não foi inicializada
// SuntaxError - Erro de sintaxe, escrevi alguma coisa errada

// log -> registro
// O  método console.log() apenas registra
// no terminal o que passamos entre os parênteses, 
// por exemplo o conteúdo de uma variável ou o resultado de uma operação.
// O JavaScript foi padronizado em 1996 pela
// European Computer Manufacturers Association (ECMA)

const minhaVar = true;
// console.log(245);
// console.log("eu sou um texto");
// console.log(minhaVar);

console.error('deu erro!');

console.log("deu erro");
console.error(new Error("deu erro"));