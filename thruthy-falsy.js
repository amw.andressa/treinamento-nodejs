// boolean
const usuarioLogado = true;
const contapaga = false;

// truthy ou falsy

// 0 => false
// 1=> true
// undefined -> espaço de memória está reservado, mas não está nada definido lá, só o nome da variável.

console.log(0 == false);
console.log("" == false);
console.log(1 == true);

let minhaVar;
let varNull = null;

console.log(minhaVar);
console.log(varNull);

let numero = 3;
let texto = "Alura";

console.log(typeof numero);
console.log(typeof texto);

// typeof -> retorna o tipo da variável.
// exemplo: console.log(typeof minhaVar)
// Lê-se assim: me retorna o tipo da variável minhaVar

// Alguns erros realmente não tem como voltar atrás.

console.log(typeof minhaVar);
console.log(typeof varNull);